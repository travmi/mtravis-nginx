# Class: nginx::config
#
# This module manages NGINX configuration files.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
class nginx::config {
  
# These will be actual files on the server. 
# We will not be using symlinks to the NFS.
# Content of these file will be populated by template.
  
  file { '/etc/nginx/nginx.conf':
    ensure  => 'file',
    content => template("nginx/nginx.conf.erb"),
    mode    => '0644',
  }
  
  file { '/etc/nginx/conf.d/default.conf':
    ensure  => 'file',
    content => template("nginx/default.conf.erb"),
    mode    => '0644',
  }

  file { '/etc/nginx/conf.d':
    ensure  => 'directory',
    recurse => true,
    purge   => true
  }  

  file { '/etc/logrotate.d/nginx':
    ensure => 'file',
    source => "puppet:///modules/nginx/nginx",
    mode   => '0644'
  }

}