# Class: nginx::vhost
#
# This module manages NGINX virtual hosts.
#
# Parameters:
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
#
define nginx::vhost (
  
  $port              = 80,
  $client_folder     = undef,
  $access_log        = undef,
  $error_log         = undef,
  $url               = undef,
  $proxy_pass        = 'http://127.0.0.1:8080',  
  
){
  
  if ! defined(Class['nginx']) {
    fail('You must include the nginx base class before using any apache defined resources')
  }  
  
  validate_string($url)
  validate_string($client_folder)
  validate_string($error_log)
  validate_string($access_log)

  file { "/etc/nginx/conf.d/${url}.conf":
    ensure  => 'file',
    content => template("nginx/vhost.conf.erb"),
    mode    => '0644',
    notify  => Service['nginx']
  }

#  Class['::nginx'] -> Class['nginx::vhost']
    
}