# Class: nginx::params
#
# This module manages NGINX paramaters
#
# Parameters:
#
# Parameters are called from Hiera and if no value is found it will use the default value.
#
# Actions:
#
# Requires:
#
# Sample Usage:
#
# This class file is not called directly
#
class nginx::params {
  
  $nx_port          = hiera('nginx::port', '80')
  $nx_client_folder = hiera('nginx::client_folder', 'test')
  $nx_access_log    = hiera('nginx::access_log', 'access_log')
  $nx_error_log     = hiera('nginx::error_log', 'error_log')
  $nx_url           = hiera('nginx::url', 'test.wcw.local')
  $nx_proxy_pass    = hiera('nginx::proxy_pass', 'http://127.0.0.1:8080')
  $nx_server_name   = hiera('nginx::server_name', 'test')
     
}