# Class: nginx::package
#
# This module manages NGINX package
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
class nginx::package {
  
  package { 'nginx':
    ensure => '1.6.0-1.el6.ngx',
  }
  
}