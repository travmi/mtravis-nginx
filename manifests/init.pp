# === Class: nginx
#
# This module manages nginx
#
# Parameters: 
#
# [*port*]
# This is the port that NGINX will listen on. This is part of the vhost template.
#
# [*client_folder*]
# This variable is part of the vhost file template 
# Example /var/www/@client_folder/@url/htdocs
#
# [*access_log*]
# Access log file name
#
# [*error_log*]
# Error log file name
#
# [*url*]
# URL of the site. This is also the folder name for the site.
#
# [*proxy_pass*]
# This will be the ip and port of where NGINX is to send data to Apache
# Example: http://127.0.0.1:8080
#
# [*server_name*]
# This variable is an array for server name. 
# Default: test.wcw.local
#
# Actions:
#
# Requires: see Modulefile
#
# Sample Usage:
#
class nginx (
  
  $manage_repo   = true
   
) inherits nginx::params{
 
  validate_bool($manage_repo)
  
# Need to move this to params class at some point. 
   if $manage_repo {
    yumrepo { 'nginx':
      ensure   => 'present',
      baseurl  => 'http://nginx.org/packages/centos/6/$basearch/',
      descr    => 'nginx repo',
      enabled  => '1',
      gpgcheck => '0',
      before   => Package['nginx'],
    }
  }

# Using anchor until I can figure out the "containment" 
  anchor { 'nginx::begin': } ->
    class  { '::nginx::package': } ->
    class  { '::nginx::config': } ~>
    class  { '::nginx::service': } ->
  anchor { 'nginx::end': }
     
}
