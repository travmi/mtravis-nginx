# Class: nginx::service
#
# This module manages NGINX services.
#
# Parameters:
#
# There are no default parameters for this class.
#
# Actions:
#
# Requires:
#
# Sample Usage:
class nginx::service {
  
  service { 'nginx':
    ensure => 'running',
    enable => true,
  }
   
}
