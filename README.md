#NGINX

####Table of Contents

1. [Overview](#overview)
2. [Module Description - What the module does and why it is useful](#module-description)
3. [Setup - The basics of getting started with nginx](#setup)
    * [What nginx affects](#what-nginx-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with nginx](#beginning-with-nginx)
4. [Usage - Configuration options and additional functionality](#usage)
5. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)
7. [License - License information](#license)

##Overview

The NGINX module installs, configures, and manages the NGINX service.

##Module Description

The NGINX module handles installing, configuring, and running NGINX on CentOS 6.5+.	 

##Setup

###What nginx affects
* nginx package.
* nginx configuration file.
* nginx service.

###Beginning with NGINX

`include '::nginx'` is not enough to get you up and running.  You need to specify
paramaters to use in the class.

```puppet
class { '::nginx':
  $url => [ 'test.wcw.local' ],
}
```

##Usage

All interaction with the nginx module can do be done through the main nginx class.

###I just want NGINX, what's the minimum I need?

```puppet
include '::nginx'
```

##Reference

###Classes

####Public Classes

* nginx: Main class, includes all other classes.

####Private Classes

* nginx::install: Handles the packages.
* nginx::config: Handles the configuration files.
* nginx::service: Handles the service.
* nginx::vhost: Handles the virtual hosts file.
* nginx::params: Handles all the variables for the classes.

###Parameters

The following parameters are available in the nginx module:

####`port`

####`client_folder`

####`access_log`

####`error_log `

####`url`

####`proxy_pass`

####`server_name`

##Limitations

This module has been built on and tested against Puppet 3.3 and higher.

The module has been tested on:

* CentOS 6

Testing on other platforms has not been. 

##Development

###License
Copyright (C) 2014 Mike Travis

For the relevant commits Copyright (C) by the respective authors.

Can be contacted at: mike.r.travis@gmail.com

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.